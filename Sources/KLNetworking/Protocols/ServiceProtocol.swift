//
//  ServiceProtocol.swift
//

import Foundation

public typealias Headers = [HeaderKey: String]

public protocol ServiceProtocol {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var task: NetworkTask { get }
    var headers: Headers? { get }
    var parametersEncoding: ParametersEncoding { get }
}
