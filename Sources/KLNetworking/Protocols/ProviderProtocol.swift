//
//  ProviderProtocol.swift
//

import Foundation

public protocol ProviderProtocol {
    var isRequestLoggerActive: Bool { get set }
    var isResponseLoggerActive: Bool { get set }

    @discardableResult
    func request<T>(service: ServiceProtocol, completion: @escaping (Result<T, NetworkError>) -> Void) -> URLSessionDataTask? where T: Decodable

    @discardableResult
    func upload<T>(service: ServiceProtocol, completion: @escaping (Result<T, NetworkError>) -> Void) -> URLSessionDataTask? where T: Decodable
}
