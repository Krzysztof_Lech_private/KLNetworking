//
//  URLSessionProtocol.swift
//

import Foundation

public protocol URLSessionProtocol {
    typealias DataTaskResult = (Data?, URLResponse?, Error?) -> Void

    func dataTask(request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTask
    func uploadTask(request: URLRequest, data: Data, completionHandler: @escaping DataTaskResult) -> URLSessionDataTask
}

extension URLSession: URLSessionProtocol {
    
    public func dataTask(request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTask {
        return dataTask(with: request, completionHandler: completionHandler)
    }

    public func uploadTask(request: URLRequest, data: Data, completionHandler: @escaping DataTaskResult) -> URLSessionDataTask {
        return uploadTask(with: request, from: data, completionHandler: completionHandler)
    }
}
