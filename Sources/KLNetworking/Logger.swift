//  Logger.swift
//  Created by Krzysztof Lech on 17/12/2022.

import Foundation

struct Logger {
    private enum LogType {
        case text, error, request, response

        var icon: String {
            switch self {
            case .text: return "🔤"
            case .error: return "❌"
            case .request: return "➡️"
            case .response: return "⬇️"
            }
        }
    }

    static private var logFooter: String {
        return "----------------------------------------\n"
    }

    private static func getLogHeader(_ logType: LogType) -> String {
        let date = Utils.currentDateAndTime
        let icon = logType.icon
        return "\n-------- \(icon) \(date) --------"
    }

    static func log(text: String?) {
        #if !DEBUG
            return
        #endif

        guard let text = text else { return }

        print(getLogHeader(.text))
        print(text)
        print(logFooter)
    }

    static func log(error: Error?, withText text: String) {
        #if !DEBUG
            return
        #endif

        let errorString = error?.localizedDescription ?? ""

        print(getLogHeader(.error))
        print("‼️", text, errorString)
        print(logFooter)
    }

    static func log(request: URLRequest) {
        #if !DEBUG
            return
        #endif

        print(getLogHeader(.request))

        if let method = request.httpMethod, let url = request.url {
            print(method, url)
        }

        if let parameters = request.allHTTPHeaderFields, !parameters.isEmpty {
            print("headers:")
            parameters.forEach { print("・", $0.key, ":", $0.value) }
        }

        if let body = request.httpBody {
            print("body:")
            print(body)
        }

        print(logFooter)
    }

    static func log(request: URLRequest, data: Data?, response: URLResponse?, error: Error?) {
        #if !DEBUG
            return
        #endif

        print(getLogHeader(.response))

        // method + url
        if let method = request.httpMethod, let url = request.url {
            print(method, url)
        }

        // status code
        if let httpResponse = response as? HTTPURLResponse {
            let statusCode = httpResponse.statusCode
            var statusCodeIcon = ""
            switch httpResponse.statusCode {
            case 200..<300 : statusCodeIcon = "🙂"
            case 400..<600 : statusCodeIcon = "👿"
            default: statusCodeIcon = ""
            }

            print("Status code:", statusCode, statusCodeIcon)
        }

        // error
        if let networkError = error as? NetworkError {
            print("‼️", networkError.errorDescription)
        } else if let error {
            print("‼️", error.localizedDescription)
        }

        // headers
        if let httpResponse = response as? HTTPURLResponse {
            print("headers:")
            httpResponse.allHeaderFields.forEach { print("・", $0.key, ":", $0.value) }
        }

        // data
        if let data = data {
            if let dataDictionary = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any], !dataDictionary.isEmpty {
                print("📎 data:")
                print(dataDictionary)
            } else if let dataArray = try? JSONSerialization.jsonObject(with: data, options: []) as? [Any], !dataArray.isEmpty {
                print("📎 data:")
                print(dataArray)
            }
        }

        print(logFooter)
    }
}
