//
//  EncodableExtension.swift
//

import Foundation

extension Encodable {
    internal func toJSONData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
}
