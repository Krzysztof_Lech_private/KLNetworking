//
//  URLRequestExtension.swift
//

import Foundation

extension URLRequest {
    internal init(service: ServiceProtocol) {
        let urlComponents = URLComponents(service: service)
        self.init(url: urlComponents.url!)

        httpMethod = service.method.rawValue
        service.headers?.forEach { key, value in
            addValue(value, forHTTPHeaderField: key.rawValue)
        }

        if case let .requestParameters(parameters) = service.task, service.parametersEncoding == .json {
            addValue("application/json", forHTTPHeaderField: HeaderKey.contentType.rawValue)
            httpBody = try? JSONSerialization.data(withJSONObject: parameters)
            
        } else if case let .request(encodable) = service.task {
            addValue("application/json", forHTTPHeaderField: HeaderKey.contentType.rawValue)
            httpBody = encodable.toJSONData()
        }
    }
}
