// swiftlint:disable:this file_name
// JSONDecoder+Extension.swift
//
// Created by Michal Czupryna on 10/08/2022.
// 

import Foundation

public extension JSONDecoder.DateDecodingStrategy {
    
    static let iso8601withFractionalSeconds = custom {
        let container = try $0.singleValueContainer()
        let string = try container.decode(String.self)
        guard let date = Formatter.iso8601withFractionalSeconds.date(from: string) else {
            throw DecodingError.dataCorruptedError(
                in: container,
                debugDescription: "Invalid date: " + string
            )
        }
        return date
    }

    static let iso8601withFractionalSecondsOptional = custom {
        let container = try $0.singleValueContainer()
        let string = try container.decode(String.self)
        guard let date = Formatter.iso8601withFractionalSeconds.date(from: string) else {
            guard let dateOptional = Formatter.iso8601.date(from: string) else {
                throw DecodingError.dataCorruptedError(
                    in: container,
                    debugDescription: "Invalid date: " + string
                )
            }
            return dateOptional
        }
        return date
    }
}

extension ISO8601DateFormatter {
    internal convenience init(_ formatOptions: Options, timeZone: TimeZone? = TimeZone(identifier: "UTC")) {
        self.init()
        self.formatOptions = formatOptions
        self.timeZone = timeZone
    }
}

extension Formatter {
    internal static let iso8601withFractionalSeconds = ISO8601DateFormatter([.withInternetDateTime, .withFractionalSeconds])
    internal static let iso8601 = ISO8601DateFormatter([.withInternetDateTime])
}
