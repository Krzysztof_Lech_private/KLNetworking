//
//  URLSessionProvider.swift
//

import Foundation

public final class URLSessionProvider: ProviderProtocol {
    private enum Constants {
        static let errorDescription = "cancelled"
    }

    private var session: URLSessionProtocol
    private var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy
    private var previousTask: URLSessionDataTask?
    private var previousService: ServiceProtocol?

    private var previousUploadTask: URLSessionDataTask?
    private var previousUploadService: ServiceProtocol?

    private lazy var jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = self.dateDecodingStrategy
        return decoder
    }()

    /// Set 'true' to log request
    public var isRequestLoggerActive = false

    /// Set 'true' to log response
    public var isResponseLoggerActive = false

    public init(session: URLSessionProtocol = URLSession.shared, dateDecodingStrategy: JSONDecoder.DateDecodingStrategy = .iso8601withFractionalSeconds) {
        self.session = session
        self.dateDecodingStrategy = dateDecodingStrategy
    }

    // MARK: - Public -

    @discardableResult
    public func request<T: Decodable>(service: ServiceProtocol, completion: @escaping (Result<T, NetworkError>) -> Void) -> URLSessionDataTask? {
        let request = URLRequest(service: service)
        if isRequestLoggerActive { Logger.log(request: request) }

        let task = session.dataTask(request: request, completionHandler: { [weak self] data, response, error in
            guard error?.localizedDescription != Constants.errorDescription else { return }
            let httpResponse = response as? HTTPURLResponse
            self?.handleDataResponse(request: request, data: data, response: httpResponse, error: error, completion: completion)
        })

        if previousService?.path == service.path { previousTask?.cancel() }

        task.resume()

        previousTask = task
        previousService = service

        return task
    }

    @discardableResult
    public func upload<T: Decodable>(service: ServiceProtocol, completion: @escaping (Result<T, NetworkError>) -> Void) -> URLSessionDataTask? {
        let request = URLRequest(service: service)
        if isRequestLoggerActive { Logger.log(request: request) }

        guard case let NetworkTask.upload(bodyData) = service.task else {
            if isRequestLoggerActive { Logger.log(request: request, data: nil, response: nil, error: NetworkError.invalidRequest) }
            completion(.failure(.invalidRequest))
            return nil
        }

        let task = session.uploadTask(request: request, data: bodyData) { [weak self] data, response, error in
            let httpResponse = response as? HTTPURLResponse
            self?.handleDataResponse(request: request, data: data, response: httpResponse, error: error, completion: completion)
        }

        if previousUploadService?.path == service.path { previousUploadTask?.cancel() }

        task.resume()

        previousUploadTask = task
        previousUploadService = service

        return task
    }

    // MARK: - Private -

    private func handleDataResponse<T: Decodable>(request: URLRequest, data: Data?, response: HTTPURLResponse?, error: Error?, completion: (Result<T, NetworkError>) -> Void) {
        if let error = error {
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: response, error: NetworkError.unknown(error)) }
            return completion(.failure(.unknown(error)))
        }

        guard let response = response else {
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: response, error: NetworkError.invalidResponse) }
            return completion(.failure(.invalidResponse))
        }

        switch response.statusCode {
        case 200 ... 299:
            self.decodeDataResponse(request: request, data: data, completion: completion)
        case 400 ... 499:
            self.decodeErrorResponse(request: request, data: data, response: response, completion: completion)
        default:
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: response, error: NetworkError.unknown(error)) }
            completion(.failure(.unknown(nil)))
        }
    }

    private func decodeDataResponse<T: Decodable>(request: URLRequest, data: Data?, completion: (Result<T, NetworkError>) -> Void) {
        guard let data = data else {
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: nil, error: NetworkError.noData) }
            return completion(.failure(.noData))
        }

        do {
            let model = try self.jsonDecoder.decode(T.self, from: data)
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: nil, error: nil) }
            completion(.success(model))

        } catch let decodingError as DecodingError {
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: nil, error: NetworkError.parseJSON(decodingError)) }
            completion(.failure(.parseJSON(decodingError)))

        } catch {
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: nil, error: NetworkError.unknown(error)) }
            completion(.failure(.unknown(error)))
        }
    }

    private func decodeErrorResponse<T>(request: URLRequest, data: Data?, response: HTTPURLResponse, completion: (Result<T, NetworkError>) -> Void) {
        if let data = data, let message = String(data: data, encoding: String.Encoding.utf8) {
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: nil, error: NetworkError.api(statusCode: response.statusCode, message: message)) }
            completion(.failure(.api(statusCode: response.statusCode, message: message)))

        } else {
            if isResponseLoggerActive { Logger.log(request: request, data: data, response: nil, error: NetworkError.api(statusCode: response.statusCode, message: nil)) }
            completion(.failure(.api(statusCode: response.statusCode, message: nil)))
        }
    }
}
