//
//  NetworkTask.swift
//

import Foundation

public typealias NetworkParameters = [String: Any]

public enum NetworkTask {
    case requestPlain
    case requestParameters(NetworkParameters)
    case request(Encodable)
    case upload(Data)
}
