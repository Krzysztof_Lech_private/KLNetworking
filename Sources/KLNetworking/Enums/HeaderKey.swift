//
//  HeaderKey.swift
//

import Foundation

public struct HeaderKey: RawRepresentable, Hashable {
    
    public var rawValue: String
    public init(rawValue: String) {
        self.rawValue = rawValue
    }

    public static let authorization = HeaderKey(rawValue: "Authorization")
    public static let contentType = HeaderKey(rawValue: "Content-Type")
    public static let accept = HeaderKey(rawValue: "Accept")
    public static let contentLength = HeaderKey(rawValue: "Content-Length")
}
