//  Utils.swift
//  Created by Krzysztof Lech on 17/12/2022.

import Foundation

internal struct Utils {

    static var currentDateAndTime: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: Date())
    }
}
