//
//  AppleApiService.swift
//

import Foundation
import KLNetworking

enum AppleApiService: ServiceProtocol {
    case softwareWithName(name: String)

    var baseURL: URL {
        return URL(string: "https://itunes.apple.com")!
    }

    var path: String {
        switch self {
        case .softwareWithName:
            return "search"
        }
    }

    var method: HTTPMethod {
        return .get
    }

    var task: NetworkTask {
        switch self {
        case .softwareWithName(let name):
            let parameters: NetworkParameters = [
                "term": name,
                "entity": "software"
            ]
            return .requestParameters(parameters)
        }
    }

    var headers: Headers? {
        let headers = Headers()
        return headers
    }

    var parametersEncoding: ParametersEncoding {
        switch self {
        case .softwareWithName: return .url
        }
    }
}
