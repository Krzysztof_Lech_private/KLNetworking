//  KLNetworkingTestApp.swift
//  Created by Krzysztof Lech on 17/12/2022.

import SwiftUI

@main
struct KLNetworkingTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: ContentViewModel())
        }
    }
}
