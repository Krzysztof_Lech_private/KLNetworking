//  ContentViewModel.swift
//  Created by Krzysztof Lech on 17/12/2022.

import Foundation
import KLNetworking

internal class ContentViewModel: ObservableObject {

    @MainActor @Published var items: [AppleItunesItem] = []

    private let apiService: AppleApiServiceWorkerProtocol = AppleApiServiceWorker(
        logRequest: true,
        logResponse: false
    )

    func actionWithCompletion() {
        let searchText = "Angry Birds"
        
        apiService.fetchSoftwareData(withText: searchText) { response in
            switch response {
            case .success(let data):
                Task { @MainActor in self.items = data.results }
                print("👍: Downloaded \(data.resultCount) items")
            case .failure(let error):
                print("🤡", error.errorDescription)
            }
        }
    }

    func asyncAwaitAction() {
        let searchText = "Angry Birds"

        Task {
            do {
                let response = try await apiService.fetchSoftwareData(withText: searchText)
                Task { @MainActor in self.items = response.results }
                print("👍: Downloaded \(response.resultCount) items")
            } catch let networkError as NetworkError {
                print("🤡", networkError.errorDescription)
            }
        }
    }
}
