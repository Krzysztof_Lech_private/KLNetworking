//
//  AppleApiServiceWorker.swift
//

import Foundation
import KLNetworking

protocol AppleApiServiceWorkerProtocol: AnyObject {
    func fetchSoftwareData(withText text: String, completion: @escaping (Result<AppleItunesResult, NetworkError>) -> Void)
    func fetchSoftwareData(withText text: String) async throws -> AppleItunesResult
}

final class AppleApiServiceWorker: AppleApiServiceWorkerProtocol {
    private var provider: ProviderProtocol

    init(provider: ProviderProtocol = URLSessionProvider(), logRequest: Bool = true, logResponse: Bool = true) {
        self.provider = provider
        self.provider.isRequestLoggerActive = logRequest
        self.provider.isResponseLoggerActive = logResponse
    }

    /// request with Result
    func fetchSoftwareData(withText text: String, completion: @escaping (Result<AppleItunesResult, NetworkError>) -> Void) {
        let service = AppleApiService.softwareWithName(name: text)
        provider.request(service: service, completion: completion)
    }

    /// async / await request
    func fetchSoftwareData(withText text: String) async throws -> AppleItunesResult {
        let service = AppleApiService.softwareWithName(name: text)
        return try await withUnsafeThrowingContinuation { continuation  in
            provider.request(service: service) { result in
                continuation.resume(with: result)
            }
        }
    }
}
