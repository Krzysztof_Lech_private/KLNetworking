//
//  AppleItuneSoftware.swift
//

import Foundation

struct AppleItunesResult: Decodable {
    let resultCount: Int
    let results: [AppleItunesItem]
}

struct AppleItunesItem: Decodable, Identifiable {
    let trackName: String
    let trackId: Int

    var id: Int { trackId }
}
