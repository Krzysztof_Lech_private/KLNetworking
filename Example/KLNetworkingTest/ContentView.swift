//  ContentView.swift
//  Created by Krzysztof Lech on 17/12/2022.

import SwiftUI

struct ContentView: View {
    @StateObject var viewModel: ContentViewModel

    var body: some View {
        VStack(alignment: .center, spacing: 8) {
            Button("Call request with completion") {
                viewModel.actionWithCompletion()
            }
            .buttonStyle(BorderedProminentButtonStyle())

            Button("Call async/await request") {
                viewModel.asyncAwaitAction()
            }
            .buttonStyle(BorderedProminentButtonStyle())

            List(viewModel.items) { item in
                Text(item.trackName)
            }
        }
    }
}

#Preview {
    ContentView(viewModel: ContentViewModel())
}
